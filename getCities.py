#!/usr/bin/env python3
#Beautiful Soup demo in Python3
#Foaad Khosmood / Spring 2017

import re, requests
from bs4 import BeautifulSoup
    
def getNicknames():
  url = "https://en.wikipedia.org/wiki/List_of_city_nicknames_in_California"
  request = requests.get(url)
  soup = BeautifulSoup(request.text,"html.parser")
  content = soup.find('div',attrs={'id':'mw-content-text'})
  abbrvs = {}

  for city in content.find_all('a', title=True):
    #print("!!!!Title ",city['title'])
    match = re.search(",", city['title'])
    match2 = re.search("Los Angeles", city['title'])
    match3 = re.search("San Diego", city['title'])
    match4 = re.search("San Francisco", city['title'])
    if match or match2 or match3 or match4:
      cityName = city.text.strip()
      li = city.find_previous('li')
      print(cityName)
      if '-' in li.text.strip():
        n = li.text.replace("-","").replace(cityName, "", 1).strip()
        n2 = re.sub("\[(\d*)\]", "", n)
        nickname = re.sub(r'\([^)]*\)', '', n2)
        print(nickname.strip())
        abbrvs[nickname.lower().strip()] = cityName.lower()
      else:
        names = li.find_next('ul')
        for name in names.find_all('li'):
          n = re.sub("\[(\d*)\]", "", name.text.strip())
          nickname = re.sub(r'\([^)]*\)', '', n)
          print(nickname.strip())
          abbrvs[nickname.lower().strip()] = cityName.lower()
      print("\n")
  return abbrvs

def getCities():
  url = "http://www.calif.com/california-cities.php"
  request = requests.get(url)
  soup = BeautifulSoup(request.text,"html.parser")
  
  c = soup.find('table')
  cities = []
  for city in c.find_all('a'):
    name = city.text.strip()
    cities.append(name.lower()) 
  return cities
