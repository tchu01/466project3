import re
import nltk.tokenize
import inverted_index


def load_orgs():
    orgs = set()
    with open('./data/dd_organizations.tsv') as org_file:
        for line in org_file.readlines()[1:]:
            name = line.split('\t')[1].strip('\n').strip()
            parans = re.search('\(.*?\)', name)
            if parans:
                paran_name = parans.group()[1:-1]
                if paran_name != '' and len(paran_name) > 1:
                    orgs.add(paran_name.lower())

            name = re.sub('\(.*?\)', '', name)
            name = re.sub('\s\s+', ' ', name)
            name = re.sub('\(.*?$', '', name)
            name = re.sub('.*?\)\s*', '', name)
            name = re.sub('\?', '', name)
            name = re.sub('\|', '', name)
            #name = re.sub('\.', '\\.', name)
            name = re.sub('\.', '', name)
            check = True
            for item in name.split('/'):
                if item != '' and len(item) > 1:
                  orgs.add(item.lower())
    return list(orgs)


def load_utterances():
    utterances = []
    with open('./data/committee_utterances2.tsv') as utt_file:
        for line in utt_file.readlines()[1:]:
            values = line.split('\t')
            utterances.append({
                'last': values[9],
                'first': values[10],
                'text': values[14]
            })

    return utterances


# Returns a list of tuples, one for each org pulled from the utterance
# Tuple contains(orgName, surrounding words[], distance to name, is complete name)
# Surrounding words is [2 to left, 1 to left, 1 to right, 2 to right]
# Each index contains tuple (WORD, POS) where POS is code from nltk pos_tag system or empty string
# Is always 4 elements, empty string if DNE
def make_tuples(utterance, invInd):
    tokens = nltk.word_tokenize(utterance['text'].lower())
    nameIndex = -1
    index = 0
    length = len(tokens)
    temp_tuples = []
    tuples = []

    # Iterate over tokens
    while index < length:

        # Get name index
        if tokens[index] == utterance['first'].lower():
            nameIndex = index
        # Recognized word in index
        elif tokens[index] in invInd.keys():
            currInd = invInd[tokens[index]]
            orgName = currInd['textValue']
            index2 = index + 1
            words = []

            # Iterate over following tokens, attempt to travel further down index
            while index2 < length and tokens[index2] in currInd.keys():
                currInd = currInd[tokens[index2]]
                index2 += 1
                orgName = orgName + ' ' + currInd['textValue']
            # Get surrounding words
            words.append(tokens[index - 2] if index > 1 else '')
            words.append(tokens[index - 1] if index > 0 else '')
            words.append(tokens[index2] if index2 < length else '')
            words.append(tokens[index2 + 1] if index2 + 1 < length else '')
            temp_tuples.append((index, index2, orgName, words, currInd['isEnd']))
                            
        index += 1

    pos_tags = nltk.pos_tag(tokens)
    # Calculate distance to name and add POS tags to surrounding words
    for t in temp_tuples:
        if nameIndex >= 0:
            distanceToName = min(abs(t[0] - nameIndex), abs((t[1] - 1) - nameIndex))
        else:
            distanceToName = -1

        words = []
        words.append(pos_tags[t[0] - 2] if len(t[3][0]) > 0 else (t[3][0], ''))
        words.append(pos_tags[t[0] - 1] if len(t[3][1]) > 0 else (t[3][1], ''))
        words.append(pos_tags[t[1]] if len(t[3][2]) > 0 else (t[3][2], ''))
        words.append(pos_tags[t[1] + 1] if len(t[3][3]) > 0 else (t[3][3], ''))
        tuples.append((t[2], words, distanceToName, t[4]))

    return tuples


def main():
    orgs = sorted(load_orgs(), key=lambda x: len(x), reverse=True)
    utterances = load_utterances()
    invInd = inverted_index.make_index(orgs)

    for utterance in utterances:
        name = utterance['first'] + ' ' + utterance['last']
        if name in utterance['text'] or utterance['first'] in utterance['text'] or utterance['last'] in utterance['text']:
            tuples = make_tuples(utterance, invInd)
            for t in tuples:
                if t[3]:
                    print(name, ' ', t)


if __name__ == "__main__":
    main()
