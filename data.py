import re
import pprint
import random
import collections
import pickle
import tqdm
import inverted_index
import make_tuples


def load_orgs():
    orgs = set()
    with open('./data/dd_organizations.tsv') as org_file:
        for line in org_file.readlines()[1:]:
            name = line.split('\t')[1].strip('\n').strip()
            parans = re.search('\(.*?\)', name)
            if parans:
                paran_name = parans.group()[1:-1]
                if paran_name != '' and len(paran_name) > 1:
                    orgs.add(paran_name.lower())

            name = re.sub('\(.*?\)', '', name)
            name = re.sub('\s\s+', ' ', name)
            name = re.sub('\(.*?$', '', name)
            name = re.sub('.*?\)\s*', '', name)
            name = re.sub('\?', '', name)
            name = re.sub('\|', '', name)
            # name = re.sub('\.', '\\.', name)
            name = re.sub('\.', '', name)

            if 'am/pm' in line.lower():
                orgs.add(item.lower())
            else:
                for item in name.split('/'):
                    if item != '' and len(item) > 1:
                        orgs.add(item.lower())

    return list(orgs)


def pickle_load(filename):
    return pickle.load(open(filename, 'rb'))


def load_scoped_utterances():
    utterances = []
    with open('./data/committee_utterances2.tsv') as utt_file:
        for line in utt_file.readlines()[1:]:
            values = line.split('\t')
            info = {'last': values[9].lower(), 'first': values[10].lower(), 'text': values[14].lower()}
            name = info['first'] + ' ' + info['last']
            if name in info['text'] or re.search(r'\b' + info['first'] + r'\b', info['text']) or re.search(r'\b' + info['last'] + r'\b', info['text']):
                utterances.append(info)
    return utterances


def count_top_words():
    left_left = collections.defaultdict(int)
    left = collections.defaultdict(int)
    right = collections.defaultdict(int)
    right_right = collections.defaultdict(int)

    utterances = load_scoped_utterances()
    random.shuffle(utterances)
    orgs = load_orgs()
    inv_idx = inverted_index.make_index(orgs)

    with open('top_words/saved.p', 'wb') as f:
        pickle.dump(utterances, f)

    for utterance in tqdm.tqdm(utterances[2000:]):
        tuples = make_tuples.make_tuples(utterance, inv_idx)
        for tup in tuples:
            if tup[3]:
                left_left[tup[1][0][0].lower()] += 1
                left[tup[1][1][0].lower()] += 1
                right[tup[1][2][0].lower()] += 1
                right_right[tup[1][3][0].lower()] += 1

    with open('top_words/left_left_invidx.p', 'wb') as f:
        pickle.dump(left_left, f)

    with open('top_words/left_invidx.p', 'wb') as f:
        pickle.dump(left, f)

    with open('top_words/right_right_invidx.p', 'wb') as f:
        pickle.dump(right_right, f)

    with open('top_words/right_invidx.p', 'wb') as f:
        pickle.dump(right, f)


def count_top_pos():
    left_left = collections.defaultdict(int)
    left = collections.defaultdict(int)
    right = collections.defaultdict(int)
    right_right = collections.defaultdict(int)

    utterances = pickle_load('top_words/saved.p')
    orgs = load_orgs()
    inv_idx = inverted_index.make_index(orgs)

    for utterance in tqdm.tqdm(utterances[2000:]):
        tuples = make_tuples.make_tuples(utterance, inv_idx)
        for tup in tuples:
            if tup[3]:
                left_left[tup[1][0][1]] += 1
                left[tup[1][1][1]] += 1
                right[tup[1][2][1]] += 1
                right_right[tup[1][3][1]] += 1

    with open('top_words/left_left_pos_invidx.p', 'wb') as f:
        pickle.dump(left_left, f)

    with open('top_words/left_pos_invidx.p', 'wb') as f:
        pickle.dump(left, f)

    with open('top_words/right_right_pos_invidx.p', 'wb') as f:
        pickle.dump(right_right, f)

    with open('top_words/right_pos_invidx.p', 'wb') as f:
        pickle.dump(right, f)


def normalize_top_words(position_dict):
    temp_list = [(key, value) for key, value in position_dict.items()]
    temp_list.sort(key=lambda x: x[1], reverse=True)
    max_count = temp_list[0][1]

    for word, count in temp_list:
        position_dict[word] = count / max_count

    return position_dict


def normalize_top_pos(position_dict):
    temp_list = [(key, value) for key, value in position_dict.items()]
    temp_list.sort(key=lambda x: x[1], reverse=True)
    max_count = temp_list[0][1]

    for pos, count in temp_list:
        position_dict[pos] = count / max_count

    return position_dict


def score_association(tup, longest_org):
    if tup[2] != 0:
        distance = 1 / abs(tup[2])
    else:
        distance = 0

    base = 1 / 100
    left_left_word_weight = base * 7
    left_word_weight = base * 7
    right_word_weight = base * 3
    right_right_word_weight = base * 3
    left_left_pos_weight = base * 7
    left_pos_weight = base * 7
    right_pos_weight = base * 3
    right_right_pos_weight = base * 3
    org_weight = base * 30
    distance_weight = base * 30

    left_left_top = normalize_top_words(pickle_load('./top_words/left_left_invidx.p'))
    left_top = normalize_top_words(pickle_load('./top_words/left_invidx.p'))
    right_top = normalize_top_words(pickle_load('./top_words/right_invidx.p'))
    right_right_top = normalize_top_words(pickle_load('./top_words/right_right_invidx.p'))

    left_left_top_pos = normalize_top_words(pickle_load('./top_words/left_left_pos_invidx.p'))
    left_top_pos = normalize_top_words(pickle_load('./top_words/left_pos_invidx.p'))
    right_top_pos = normalize_top_words(pickle_load('./top_words/right_pos_invidx.p'))
    right_right_top_pos = normalize_top_words(pickle_load('./top_words/right_right_pos_invidx.p'))

    left_left_word, left_left_pos = word_score(tup[1][0][0], tup[1][0][1], left_left_top, left_left_top_pos)
    left_word, left_pos = word_score(tup[1][1][1], tup[1][1][1], left_top, left_top_pos)
    right_word, right_pos = word_score(tup[1][2][1], tup[1][2][1],right_top, right_top_pos)
    right_right_word, right_right_pos = word_score(tup[1][3][1], tup[1][3][1],right_right_top, right_right_top_pos)

    org_score = len(tup[0].split()) / longest_org

    score = left_left_word_weight * left_left_word + left_word_weight * left_word
    score += right_word_weight * right_word + right_right_word_weight * right_right_word
    score = left_left_pos_weight * left_left_pos + left_pos_weight * left_pos
    score += right_pos_weight * right_pos + right_right_pos_weight * right_right_pos
    score += org_weight * org_score + distance_weight * distance

    return score


def word_score(word, pos, top_words, top_pos):
    if word in top_words and pos in top_pos:
        return top_words[word], top_pos[pos]
    elif word in top_words and pos not in top_pos:
        return top_words[word], 0
    elif word not in top_words and pos in top_pos:
        return 0, top_pos[pos]
    else:
        return 0, 0


def main():
    test = pickle_load('./top_words/saved.p')[2000:]
    for item in test:
        item['text'] = item['text'].lower()
        item['first'] = item['first'].lower()
        item['last'] = item['last'].lower()
    random.shuffle(test)
    test = test[:10]
    orgs = load_orgs()
    inv_idx = inverted_index.make_index(orgs)

    longest_org = [len(org.split()) for org in orgs]
    longest_org = max(longest_org)

    for item in test:
        max_list = []
        tuples = make_tuples.make_tuples(item, inv_idx)
        pprint.pprint(item['text'])
        pprint.pprint(item['first'] + ' ' + item['last'])
        print()
        for tup in tuples:
            if tup[3]:
                # pprint.pprint(tup)
                max_list.append((tup, score_association(tup, longest_org)))
                print()
        max_list.sort(key=lambda x: x[1], reverse=True)
        pprint.pprint(max_list[:3])
        print('========\n')


if __name__ == '__main__':
    main()
    # count_top_words()
    # count_top_pos()
