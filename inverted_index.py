def make_index(items):
    first_words = {}

    for item in items:
        current_word = first_words
        for i in item.split():
            if i not in current_word.keys():
                current_word[i] = {'textValue': i, 'isEnd': False}
            current_word = current_word[i]
        current_word['isEnd'] = True
    return first_words


